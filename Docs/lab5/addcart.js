describe('Tema5', () => {
  const params = require('../params/addcartP.json');
  
  params.forEach((test) => {
	it('Add To Cart', () => {
		cy.visit('https://www.saucedemo.com/')
		cy.get('#user-name').type(test.name)
		cy.get('#password').type(test.passwd)
		cy.get('#login-button').click()
		cy.get('#add-to-cart-sauce-labs-backpack').click()
		cy.get('#remove-sauce-labs-backpack').contains('Remove')
    });
	});
})