describe('Tema5', () => {
	
  const params = require('../params/flow2P.json');
  
  params.forEach((test) => {
	it('Flow 2', () => {
		cy.visit('https://www.saucedemo.com/')
		cy.contains('Accepted usernames are')
		cy.get('#user-name').type(test.name)
		cy.get('#password').type(test.passwd)
		cy.get('#login-button').click()
		cy.contains('Sauce Labs Backpack')
		cy.get('.product_sort_container').select(test.sort)
		cy.contains('Sauce Labs Fleece Jacket')
		cy.get('#add-to-cart-sauce-labs-fleece-jacket').click()
		cy.get('#remove-sauce-labs-fleece-jacket').contains('Remove')
		cy.get('.product_sort_container').select(test.sort)
		cy.contains('Sauce Labs Onesie')
		cy.get('#add-to-cart-sauce-labs-onesie').click()
		cy.get('#remove-sauce-labs-onesie').contains('Remove')
		cy.get('.shopping_cart_link').click()
		cy.contains('$7.99')
		cy.get('#checkout').click()
		cy.get('.header_secondary_container').contains('Checkout')
		cy.get('#first-name').type(test.fName)
		cy.get('#last-name').type(test.lName)
		cy.get('#postal-code').type(test.pCode)
		cy.get('#continue').click()
		cy.contains('Total: $62.62')
		cy.get('#cancel').click()
		cy.get('.shopping_cart_link').click()
		cy.contains('$49.99')
		cy.get('#remove-sauce-labs-onesie').click()
		cy.get('#remove-sauce-labs-fleece-jacket').click()
		cy.get('#continue-shopping').click()
		cy.contains('Sauce Labs Backpack')
		cy.get('#react-burger-menu-btn').click()
		cy.get('#logout_sidebar_link').click()
		cy.contains('Accepted usernames are:')
    });
	});
})