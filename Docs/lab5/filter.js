describe('Tema5', () => {
  const params = require('../params/filterP.json');
  
  params.forEach((test) => {
	it('Filter', () => {
		cy.visit('https://www.saucedemo.com/')
		cy.contains('Accepted usernames are')
		cy.get('#user-name').type(test.name)
		cy.get('#password').type(test.passwd)
		cy.get('#login-button').click()
		cy.contains('Sauce Labs Backpack')
		cy.get('.product_sort_container').select(test.choice)
		cy.contains('Test.allTheThings() T-Shirt (Red)')
    });
	});
 
})