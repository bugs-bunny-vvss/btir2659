describe('Tema5', () => {
	const params = require('../params/loginP.json');

	params.forEach((test) => {
		it('Login', () => {
			cy.visit('https://www.saucedemo.com/')
			cy.contains('Accepted usernames are')
			cy.get('#user-name').type(test.name)
			cy.get('#password').type(test.passwd)
			cy.get('#login-button').click()
			cy.contains('Sauce Labs Backpack')
    });
	});
})