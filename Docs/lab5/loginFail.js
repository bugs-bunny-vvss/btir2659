describe('Tema5', () => {
	const params = require('../params/loginFailP.json');

	params.forEach((test) => {
		it('Login', () => {
			cy.visit('https://www.saucedemo.com/')
			cy.contains('Accepted usernames are')
			cy.get('#user-name').type(test.name)
			cy.get('#password').type(test.passwd)
			cy.get('#login-button').click()
			cy.contains('Epic sadface: Username and password do not match any user in this service')
    });
	});
})