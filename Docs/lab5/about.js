describe('Tema5', () => {
  const params = require('../params/aboutP.json');
  
  params.forEach((test) => {
	it('About', () => {
		cy.visit('https://www.saucedemo.com/')
		cy.contains('Accepted usernames are')
		cy.get('#user-name').type(test.name)
		cy.get('#password').type(test.passwd)
		cy.get('#login-button').click()
		cy.contains('Sauce Labs Backpack')
		cy.get('#react-burger-menu-btn').click()
		cy.get('#about_sidebar_link').click()
		cy.url().should('include', 'https://saucelabs.com/')
    });
	});

})