package tasks.validator;

import tasks.exceptions.TaskException;
import tasks.model.Task;

import java.util.Date;

public class TaskValidator {

    public void validateTask(Task task) {
        String errors = "";
        if (task.getTime().getTime() < 0) {
            errors += "Task time cannot be negative\n";
        }
        if (task.getStartTime().getTime() < 0 || task.getEndTime().getTime() < 0) {
            errors += "Task time intervals cannot be negative\n";
        }
        if (task.getRepeatInterval() < 1) {
            errors += "Task interval sould be bigger than 1\n";
        }
        if (task.getTitle() == null) {
            errors += "Task title cannot be null\n";
        }
        if(task.getTitle()!= null && task.getTitle().equals("")){
            errors += "Task title cannot be empty\n";
        }
        if (errors.length() > 0) {
            throw new TaskException(errors);
        }

    }
}
