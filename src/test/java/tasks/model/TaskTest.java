package tasks.model;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import tasks.exceptions.TaskException;
import tasks.repository.ArrayTaskList;
import tasks.services.TasksService;
import tasks.validator.TaskValidator;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

class TaskTest {

    private Task task;
    private Task taskk;
    private Date start, end;
    private SimpleDateFormat sdf = Task.getDateFormat();

    private ArrayTaskList repo;
    private TasksService service;

    @BeforeEach
    void setUp() {
        try {
            task=new Task("new task",Task.getDateFormat().parse("2021-02-12 10:10"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Test
    void testTaskCreationFirstConstructor() throws ParseException {
       assert task.getTitle().equals("new task");
        System.out.println(task.getFormattedDateStart());
        System.out.println(Task.getDateFormat().format(Task.getDateFormat().parse("2021-02-12 10:10")));
       assert task.getFormattedDateStart().equals(Task.getDateFormat().format(Task.getDateFormat().parse("2021-02-12 10:10")));

    }

    @Test
    void testTaskCreationSecondConstructor() throws ParseException {
        start=sdf.parse("2022-03-14 12:00");
        end=sdf.parse("2022-05-24 10:00");
        taskk = new Task("A", start, end, 10);
        assert taskk.getTitle().equals("A");
        assert taskk.getRepeatInterval()== 10;
        assert taskk.getStartTime().equals(sdf.parse("2022-03-14 12:00"));
        assert taskk.getEndTime().equals(sdf.parse("2022-05-24 10:00"));
    }

    @Test
    void EntityIntegrationStep3Add() throws ParseException {
        sdf= Task.getDateFormat();
        repo = new ArrayTaskList();
        task=new Task("Prima", sdf.parse("2022-03-15 12:00"), sdf.parse("2022-05-24 10:00"), 10);
        repo.add(task);
        service = new TasksService(repo);
        assert(service.getObservableList().size()==1);
    }

    @Test
    void EntityIntegrationStep3Remove() throws ParseException{
        sdf= Task.getDateFormat();
        repo = new ArrayTaskList();
        task=new Task("Prima", sdf.parse("2022-03-15 12:00"), sdf.parse("2022-05-24 10:00"), 10);
        repo.add(task);
        assert(repo.remove(task)==true);
        service = new TasksService(repo);
        assert(service.getObservableList().size()==0);
    }



    @AfterEach
    void tearDown() {
    }
}