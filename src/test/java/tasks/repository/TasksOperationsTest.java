package tasks.repository;

import javafx.collections.ArrayChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import tasks.model.Task;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class TasksOperationsTest {

    private SimpleDateFormat sdf;
    private TasksOperations tasksOperations;
    private Date start, end;

    @BeforeEach
    void setUp() throws ParseException {
        sdf= Task.getDateFormat();
        List<Task> taskList = new ArrayList<>();
        taskList.add(new Task("Prima", sdf.parse("2022-03-15 12:00"), sdf.parse("2022-05-24 10:00"), 10));
        taskList.add(new Task("A doua", sdf.parse("2022-04-14 12:00"), sdf.parse("2022-06-25 10:00"), 10));
        taskList.add(new Task("A treia", sdf.parse("2022-03-21 12:00"), sdf.parse("2022-03-28 10:00"), 10));
        taskList.add(new Task("A patra", sdf.parse("2022-02-09 12:00"), sdf.parse("2022-02-12 10:00"), 10));
        ObservableList<Task> observableArrayList = FXCollections.observableArrayList(taskList);
        tasksOperations = new TasksOperations( observableArrayList);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void filtrare() {
    }



    @Test
    void searchWithEndBeforeStart_C01() throws ParseException {
        start=sdf.parse("2022-03-14 12:00");
        end=sdf.parse("2022-03-11 12:00");
        List<Task> tasks;
        tasks = (List<Task>) tasksOperations.filtrare(start, end);
        assert (tasks.size()==0);
    }

    @Test
    void searchWithEmptyListCorrectParameters_C02() throws  ParseException{
        start=sdf.parse("2022-03-14 12:00");
        end=sdf.parse("2022-06-24 12:00");
        List<Task> tasks;
        ObservableList<Task> observableArrayList = FXCollections.observableArrayList(new ArrayList<>());
        tasksOperations = new TasksOperations( observableArrayList);
        tasks = (List<Task>) tasksOperations.filtrare(start, end);
        assert (tasks.size()==0);
    }

    @Test
    void searchWithOneTaskNonIncomingListCorrectParameters_C03() throws  ParseException{
        start=sdf.parse("2022-03-14 12:00");
        end=sdf.parse("2022-06-24 12:00");
        List<Task> tasks;
        ArrayList<Task> oneTaskList = new ArrayList<>();
        oneTaskList.add(new Task("Prima", sdf.parse("2022-02-25 12:00"), sdf.parse("2022-02-27 10:00"), 10));
        ObservableList<Task> observableArrayList = FXCollections.observableArrayList(oneTaskList);
        tasksOperations = new TasksOperations( observableArrayList);
        tasks = (List<Task>) tasksOperations.filtrare(start, end);
        assert (tasks.size()==0);
    }

    @Test
    void searchWithCorrectParameters_C04() throws ParseException {
        List<Task> taskList = new ArrayList<>();
        Task t1 = new Task("Prima", sdf.parse("2022-03-10 12:00"), sdf.parse("2022-05-24 10:00"), 10);
        t1.setActive(true);
        taskList.add(t1);
        ObservableList<Task> observableArrayList = FXCollections.observableArrayList(taskList);
        tasksOperations = new TasksOperations( observableArrayList);
        start=sdf.parse("2022-03-14 12:00");
        end=sdf.parse("2022-06-24 12:00");
        List<Task> tasks;
        tasks = (List<Task>) tasksOperations.filtrare(start, end);
        assert (tasks.size()==1);
    }


    @Test
    void searchWithCorrectParameters_C05() throws ParseException {
        List<Task> taskList = new ArrayList<>();
        Task t1 = new Task("Prima", sdf.parse("2022-03-15 12:00"), sdf.parse("2022-05-24 10:00"), 0);
        t1.setActive(true);
        taskList.add(t1);
        ObservableList<Task> observableArrayList = FXCollections.observableArrayList(taskList);
        tasksOperations = new TasksOperations( observableArrayList);
        start=sdf.parse("2022-03-14 12:00");
        end=sdf.parse("2022-06-24 12:00");
        List<Task> tasks;
        tasks = (List<Task>) tasksOperations.filtrare(start, end);
        assert (tasks.size()==1);
    }

    @Test
    void searchWithCorrectParameters_C06() throws ParseException {
        start=sdf.parse("2022-03-14 12:00");
        end=sdf.parse("2022-06-24 12:00");
        List<Task> tasks;
        tasks = (List<Task>) tasksOperations.filtrare(start, end);
        assert (tasks.size()==0);
    }
}