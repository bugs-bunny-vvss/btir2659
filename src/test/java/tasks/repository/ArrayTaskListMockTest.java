package tasks.repository;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mockito;
import tasks.exceptions.TaskException;
import tasks.model.Task;
import tasks.services.TasksService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

class ArrayTaskListMockTest {

    private Task task;
    private Task mockTask;
    private Date start, end;
    private SimpleDateFormat sdf;
    private ArrayTaskList arrayTaskList;
    private ArrayTaskList arrayTaskListMock;

    private ArrayTaskList repo;
    private TasksService service;
    @BeforeEach
    void setUp() {
        sdf= Task.getDateFormat();
        try {
            start=sdf.parse("2022-03-14 12:00");
            end=sdf.parse("2022-05-24 10:00");
        } catch (ParseException e) {
            fail(e.getMessage());
        }
        arrayTaskList = new ArrayTaskList();
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void mockTestRepoAdd() {
        setUp();
        mockTask = mock(Task.class);
        arrayTaskListMock = mock(ArrayTaskList.class);
        Mockito.doNothing().when(arrayTaskListMock).add(mockTask);
        Mockito.when(arrayTaskListMock.getAll()).thenReturn(Arrays.asList(mockTask));
        arrayTaskListMock.add(mockTask);
        assert (arrayTaskListMock.getAll().size()==1);
    }

    @Test
    void mockTestRepoRemove() {
        setUp();
        mockTask = mock(Task.class);
        arrayTaskListMock = mock(ArrayTaskList.class);
        Mockito.when(arrayTaskListMock.remove(mockTask)).thenReturn(true);
        Mockito.when(arrayTaskListMock.getAll()).thenReturn(Arrays.asList());
        assert (arrayTaskListMock.remove(mockTask)==true);
        assert (arrayTaskListMock.getAll().size()==0);

    }

    @Test
    void RepositoryIntegrationStep2Add(){
        sdf= Task.getDateFormat();
        repo = new ArrayTaskList();
        mockTask = mock(Task.class);
        repo.addWithoutValidator(mockTask);
        service = new TasksService(repo);
        assert(service.getObservableList().size()==1);
    }

    @Test
    void RepositoryIntegrationStep2Remove(){
        sdf= Task.getDateFormat();
        repo = new ArrayTaskList();
        mockTask = mock(Task.class);
        repo.addWithoutValidator(mockTask);
        assert(repo.remove(mockTask)==true);
        service = new TasksService(repo);
        assert(service.getObservableList().size()==0);
    }



}