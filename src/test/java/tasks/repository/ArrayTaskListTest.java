package tasks.repository;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import tasks.exceptions.TaskException;
import tasks.model.Task;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class ArrayTaskListTest {

    private Task task;
    private Date start, end;
    private SimpleDateFormat sdf;
    private ArrayTaskList arrayTaskList;
    @BeforeEach
    void setUp() {
        sdf= Task.getDateFormat();
        try {
            start=sdf.parse("2022-03-14 12:00");
            end=sdf.parse("2022-05-24 10:00");
        } catch (ParseException e) {
            fail(e.getMessage());
        }
        arrayTaskList = new ArrayTaskList();
    }

    @AfterEach
    void tearDown() {
    }

    @ParameterizedTest
    @ValueSource(strings = { "Seminar", "Laborator", "Curs" })
    void addWithCorrectParameters_ECP(String title) {
        setUp();
        task = new Task(title, start, end, 10);
        arrayTaskList.add(task);
        Task addedTask = arrayTaskList.getTask(arrayTaskList.size()-1);
        assert (addedTask==task);

    }

    @RepeatedTest(value = 3, name = "{displayName} {currentRepetition}/{totalRepetitions}")
    @DisplayName("RepeatingTest")
    void addWithEmptyTitleAndNegativeInterval_ECP() {
        setUp();
        task = new Task("", start, end, -2);
        try {
            arrayTaskList.add(task);
        }
        catch (TaskException e){
            assert e.getMessage()=="Task interval sould be bigger than 1\nTask title cannot be empty\n";
        }


    }

    @Test
    @Tag("Negative_Interval")
    void addWithNegativeInterval_ECP() {
        setUp();
        task = new Task("A", start, end, -2);
        try {
            arrayTaskList.add(task);
        }
        catch (TaskException e){
            assert e.getMessage()=="Task interval sould be bigger than 1\n";
        }

    }

    @Test
    @DisplayName("Null title")
    void addWithNullTitle_ECP() {
        setUp();
        task = new Task(null, start, end, 100);
        try {
            arrayTaskList.add(task);
        }
        catch (TaskException e){
            assert e.getMessage()=="Task title cannot be null\n";
        }

    }

    @Test
    void addWithEmptyTitle_BVA() {
        setUp();
        task = new Task("", start, end, 10);
        try {
            arrayTaskList.add(task);
        }
        catch (TaskException e){
            assert e.getMessage()=="Task title cannot be empty\n";
        }

    }

    @Test
    @Timeout(100L)
    void addWithCorrectParameters_BVA() {
        setUp();
        task = new Task("T", start, end, 10);
        arrayTaskList.add(task);
        Task addedTask = arrayTaskList.getTask(arrayTaskList.size()-1);
        assert (addedTask==task);

    }

    @Test
    void addWithZeroInterval_BVA() {
        setUp();
        task = new Task("Seminar", start, end, 0);
        try {
            arrayTaskList.add(task);
        }
        catch (TaskException e){
            assert e.getMessage()=="Task interval sould be bigger than 1\n";
        }

    }

    @Test
    @Disabled
    void addWithCorrectParametersIntervalMaxLess1_BVA() {
        setUp();
        task = new Task("T", start, end, Integer.MAX_VALUE-1);
        arrayTaskList.add(task);
        Task addedTask = arrayTaskList.getTask(arrayTaskList.size()-1);
        assert (addedTask==task);

    }

    @Test
    void addWithCorrectParametersTitle255Chars_BVA() {
        setUp();
        String title="";
        for(int i=0;i<255;i++){
            title+="T";
        }
        assert (title.length()==255);
        task = new Task(title, start, end, Integer.MAX_VALUE-1);
        arrayTaskList.add(task);
        Task addedTask = arrayTaskList.getTask(arrayTaskList.size()-1);
        assert (addedTask==task);
    }



}