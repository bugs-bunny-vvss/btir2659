package tasks.services;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mockito;
import tasks.model.Task;
import tasks.repository.ArrayTaskList;
import tasks.repository.TasksOperations;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

class TasksServiceTest {

    private SimpleDateFormat sdf;
    private ArrayTaskList mockRepo;
    private TasksService service;
    private Date start, end;

    @BeforeEach
    void setUp() throws ParseException {
        sdf= Task.getDateFormat();
        mockRepo = mock(ArrayTaskList.class);
        service = new TasksService(mockRepo);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void mockTestServiceAdd() throws ParseException {
        Task t1 = new Task("Prima", sdf.parse("2022-03-15 12:00"), sdf.parse("2022-05-24 10:00"), 10);
        Mockito.doNothing().when(mockRepo).add(t1);
        Mockito.when(mockRepo.getAll()).thenReturn(Arrays.asList(t1));
        assert (service.getObservableList().size()==1);
    }

    @Test
    void mockTestServiceFilter() throws ParseException {
        Task t1 = new Task("Prima", sdf.parse("2022-03-15 12:00"), sdf.parse("2022-05-24 10:00"), 10);
        Mockito.when(mockRepo.getAll()).thenReturn(Arrays.asList(t1));
        ArrayList<Task> filterTasks = (ArrayList<Task>) service.filterTasks((sdf).parse("2021-03-15 12:00"),(sdf).parse("2023-05-24 12:00"));
        assert (filterTasks.size()==0);
    }



}